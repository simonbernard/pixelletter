# pixelletter
A node module to send letters via pixelletter.de for Node.JS.
Supports current API version 1.3.


## Features
- Send letters from raw text
- Send letters from file
- Get current account balance
- Cancel an existing order

## Installing
```
npm install pixelletter --save
```

## Usage
Load and configure as in the following example. Then use the provided
functions as listed in the features section.

```javascript
var pixelletter = require('pixelletter');

pixelletter.configure({
	email: 'yours@domain.com',
	password: 'password',
	sandbox: true
});
```

## Features
Following features are supported.

### sendText()
You can send a letter from plain text as in the following example.
Please note, that you can send paper letters aswell as fax messages.
Use the action parameter to decide what you want to send:

```javascript
action: pixelletter.LETTER; // send as paper
action: pixelletter.FAX; // send as fax
action: pixelletter.LETTER | pixelletter.FAX; // send both!
```

This is how to send a letter from plain text:

```javascript
pixelletter.sendText({
	action: pixelletter.LETTER,
	fax: '+49 2222 1111111', // only if action FAX is selected
	address: 'Bob & Alice\n123 Some street\n8000 Somecity', // multi-line address
	subject: 'Hi Bob & Alice',
	message: 'How are you?',
	destination: 'DE', // defaults to DE but can be almost any country
	color: true, // print colored letters, default false
	green: true  // send co2 neutral, default false
}, function (err, id) {
	console.log(err);
	console.log(id); // logs the id that can be used to cancel a transaction
});
```

### sendFile()
Sending a PDF or Word document is even easier.

```javascript
pixelletter.sendFile({
	action: pixelletter.LETTER,
	file: './Test.pdf',
	color: true, // print colored letters, default false
	green: true  // send co2 neutral, default false
}, function (err, id) {
	console.log(err);
	console.log(id);
});
```

### cancel()
You can cancel an transaction with the cancel() function.
Here is an example.

```javascript
pixelletter.sendFile({
	action: pixelletter.LETTER,
	file: './Test.pdf'
}, function (err, id) {
	pixelletter.cancel(id, function (error) {
		if(error)
			console.log(error);
	});
});
```

### balance()
You can get your current account balance like this:

```javascript
pixelletter.balance(function (err, balance) {
	console.log(balance);
});
```

## Tests
No automated tests available but tested!

## Unsupported
The following features are not yet supported, contributions very welcome!

* Digital signatures
* "Addoptions" like Nachnahme
* Sending postcards
* Setting an explicit return address
* maybe something else?
