'use strict';

var fs = require('fs');
var path = require('path');
var striptags = require('striptags');
var format = require('string-template');
var request = require('request');
var mime = require('mime');

var Pixelletter = function () {
	this.LETTER = 1;
	this.FAX = 2;
	this.url = 'https://www.pixelletter.de/xml/index.php';
	this.xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><pixelletter version="1.3"><auth><email>{email}</email><password>{password}</password><agb>ja</agb><widerrufsverzicht>ja</widerrufsverzicht><testmodus>{sandbox}</testmodus><ref></ref></auth>{command}</pixelletter>';
};

/**
 * Configure Pixelletter before using it.
 *
 * Example configuration:
 * {
 *	email: 'yours@domain.com',
 *	password: 'pwd',
 *	sandbox: true
 * }
 * Setting sandbox parameter to true you can test your implementation
 * and letters will not actually be send.
 *
 * @param {object} config
 */
Pixelletter.prototype.configure = function (config) {
	this.config = config;
};

/**
 * Send a letter from plain text.
 *
 * Example letter:
 * {
 *	action: pixelletter.LETTER, // or pixelletter.FAX or (pixelletter.LETTER | pixelletter.fax to send both)
 *	transaction: '9827594325', // optional transaction no.
 *	address: 'Name\nStreet 18\n11111 City', // Multi-line string
 *	fax: '+49 1234 12345678', // fax number
 *	subject: 'My letter subject',
 *	message: 'My letter content',
 *	destination: 'DE', // defaults to 'DE'
 *	color: true, // print colored letters
 *	green: true, // send co2 neutral
 * }
 *
 * @param {object} letter
 * @param {function} callback is called with an optional error and the pixelletter order id on success.
 */
Pixelletter.prototype.sendText = function (letter, callback) {
	callback = callback || console.log;
	// action in 1,2,3
	if (letter.action < 1 || letter.action > 3)
		throw new Error('Action should be between 1 and 3');
	letter.transaction = striptags(letter.transaction);
	letter.address = striptags(letter.address);
	letter.subject = striptags(letter.subject);
	letter.message = striptags(letter.message);
	letter.destination = letter.destination || 'DE';
	letter.addoption = '';
	if (letter.color)
		letter.addoption += '33';
	if (letter.green)
		letter.addoption += ',44';
	var command = format('<command><order type="text"><options><action>{action}</action><transaction>{transaction}</transaction><control></control><fax>{fax}</fax><location></location><destination>{destination}</destination><addoption>{addoption}</addoption><returnaddress></returnaddress></options><text><address>{address}</address><subject>{subject}</subject><message>{message}</message></text></order></command>',
		letter
		);
	var formData = {
		xml: format(this.xml, {
			email: this.config.email,
			password: this.config.password,
			sandbox: this.config.sandbox,
			command: command
		})
	};
	request.post({url: this.url, formData: formData}, function (err, response, body) {
		if (err)
			return callback(err);
		if (body.indexOf('<result code="100">') < 0)
			return callback(new Error('Received unsuccessfull return code: \n' + body));
		return callback(null, body.match(/<id>([0-9]+)<\/id>/)[1]);
	});
};

/**
 * Send a letter from an attachment.
 *
 * Example letter:
 * {
 *	action: pixelletter.LETTER, // or pixelletter.FAX or (pixelletter.LETTER | pixelletter.fax to send both)
 *	transaction: '9827594325', // optional transaction no.
 *	fax: '+49 1234 12345678', // fax number
 *	destination: 'DE' // defaults to 'DE',
 *	file: 'path/to/my.pdf'	// the file you want to send
 * }
 *
 * @param {object} letter
 * @param {function} callback is called with an optional error and the pixelletter order id on success.
 */
Pixelletter.prototype.sendFile = function (letter, callback) {
	callback = callback || console.log;
	// action in 1,2,3
	if (letter.action < 1 || letter.action > 3)
		throw new Error('Action should be between 1 and 3');
	// file exists?
	try {
		fs.statSync(letter.file);
	}
	catch (e) {
		throw new Error('File ' + letter.file + ' not found or no file');
	}
	letter.transaction = striptags(letter.transaction);
	letter.destination = letter.destination || 'DE';
	letter.addoption = '';
	if (letter.color)
		letter.addoption += '33';
	if (letter.green)
		letter.addoption += ',44';
	var command = format('<command><order type="upload"><options><action>{action}</action><transaction>{transaction}</transaction><control></control><fax>{fax}</fax><location></location><destination>{destination}</destination><addoption>{addoption}</addoption><returnaddress></returnaddress></options></order></command>',
		letter
		);
	var formData = {
		xml: format(this.xml, {
			email: this.config.email,
			password: this.config.password,
			sandbox: this.config.sandbox,
			command: command
		}),
		uploadfile0: {
			value: fs.createReadStream(letter.file),
			options: {
				filename: path.basename(letter.file),
				contentType: mime.lookup(letter.file)
			}
		}
	};
	request.post({url: this.url, formData: formData}, function (err, response, body) {
		if (err)
			return callback(err);
		if (body.indexOf('<result code="100">') < 0)
			return callback(new Error('Received unsuccessfull return code: \n' + body));
		return callback(null, body.match(/<id>([0-9]+)<\/id>/)[1]);
	});
};

/**
 * Cancel an order using the id returned by sendText, sendFile.
 *
 * @param {integer} id
 * @param {function} callback is called with an optional error.
 */
Pixelletter.prototype.cancel = function (id, callback) {
	callback = callback || console.log;
	var command = format('<command><order type="cancel"><id>{id}</id></order></command>',
		{id: id}
	);
	var formData = {
		xml: format(this.xml, {
			email: this.config.email,
			password: this.config.password,
			sandbox: this.config.sandbox,
			command: command
		})
	};
	request.post({url: this.url, formData: formData}, function (err, response, body) {
		if (err)
			return callback(err);
		if (body.indexOf('<result code="100">') < 0)
			return callback(new Error('Received unsuccessfull return code: \n' + body));
		return callback(null);
	});
};

/**
 * Get current account balance.
 *
 * @param {function} callback is called with an optional error and current account balance as float.
 */
Pixelletter.prototype.balance = function (callback) {
	callback = callback || console.log;
	var command = '<command><info><account:info type="all"/></info></command>';
	var formData = {
		xml: format(this.xml, {
			email: this.config.email,
			password: this.config.password,
			sandbox: this.config.sandbox,
			command: command
		})
	};
	request.post({url: this.url, formData: formData}, function (err, response, body) {
		if (err)
			return callback(err);
		if (body.indexOf('<customer:id>') < 0)
			return callback(new Error('Received unexpected response: \n' + body));
		return callback(null, parseFloat(body.match(/<customer:credit.*>(.+)<\/customer.credit>/)[1]));
	});
};

module.exports = new Pixelletter();
